export default{
 errors:{
    networkError:"ネットワークエラーが発生しました"
 },
 translateErrorMessage:function(message){
    console.log(message.message)
    switch(message.message){
        case 'Network error. Check DevTools console for more information.':
            return this.errors.networkError
    }
      
 }
}