export default {
  emailRegex:/^[A-Za-z0-9]/,
  passwordRegex:/^[a-z0-9]+$/,
  errorMesseages:{
    email:"メールアドレスは英数字で入力してください",
    password:"パスワードは小文字英数字で入力してください",
  },
  emailValidate:function(inputBlank,errors){
    if (inputBlank !== "" && !this.emailRegex.test(inputBlank)) {
      errors.push(this.errorMesseages.email);
     }
  },
  passwordValidate:function(inputBlank,errors){
    if (inputBlank !== "" && !this.passwordRegex.test(inputBlank)) {
      errors.push(this.errorMesseages.password);
     }
  },
};
