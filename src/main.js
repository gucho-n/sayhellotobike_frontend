{/* <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDBFLE1yidUBaXXmsVjS31lOVKyUaqIPlk&callback=initMap"></script> */}
import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import App from './App.vue'
import router from './router'

const app = createApp(App).use(router)
app.use(ElementPlus)
app.mount('#app')
