import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/resister',
    name: 'resister',
    component: () => import(/* webpackChunkName: "about" */ '../views/ResisterView.vue')
  },
  {
    path: '/welcome',
    name:'welcomeForm',
    component: () => import(/* webpackChunkName: "about" */ '../views/WelcomeView.vue')

  },
  {
    path: '/map',
    name:'map',
    component: () => import(/* webpackChunkName: "about" */ '../views/MapView.vue')

  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
